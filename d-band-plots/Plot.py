import pandas as pd
import seaborn as sns
E_PBE = [0.08, 0.70, 1.3, 0.66, 2.16, -0.23, 1.92]
E_HSE = [-0.23, 0.64, 0.73, 0.65, 1.88, -0.47, 1.56]
ε_d_ε_f = [.99, 1.38, 1.43, -0.17, 1.71, 0.74, 1.94]
ε_d_ε_f_ = [.16, .35, .64, -1.26, .93, -.08, 1.23]
E_data = list(zip(E_PBE, E_HSE, ε_d_ε_f, ε_d_ε_f_))
E_ddata = pd.DataFrame(E_data, columns = ['PBE Adsorption Energy (eV)','HSE06 Adsorption Energy (eV)', 'ε$_d$-ε$_f$ (eV)', 'ε$_d$-ε$_f$ (10 eV to 0 eV) (eV)'])
plot1 = E_ddata.plot(x = 'ε$_d$-ε$_f$ (eV)', y= 'HSE06 Adsorption Energy (eV)', kind = 'scatter')
plot2 = E_ddata.plot(x = 'ε$_d$-ε$_f$ (10 eV to 0 eV) (eV)', y= 'HSE06 Adsorption Energy (eV)', kind = 'scatter')
plot3 = E_ddata.plot(x = 'ε$_d$-ε$_f$ (10 eV to 0 eV) (eV)', y= 'PBE Adsorption Energy (eV)', kind = 'scatter')
plot1.figure.savefig('plot1.png', dpi = 300)
plot2.figure.savefig('plot2.png', dpi = 300)
plot3.figure.savefig('plot3.png', dpi = 300)