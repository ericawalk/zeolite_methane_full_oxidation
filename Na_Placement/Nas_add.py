from ase import atom , Atoms
from ase import neighborlist
from ase.io.vasp import write_vasp
from ase.io import read
from scipy import sparse
import numpy as np
from numpy import linalg as LA
import sys
import os
import os.path
import shutil
from os.path import join
import re
import pandas as pd
import math
np.set_printoptions(threshold=sys.maxsize) #just for debugging

cwd = os.getcwd()
os.chdir("/projects/academic/ericwalk/kevingie/SSZ13/H/Structures/2Al/PBE")
cwd2 = os.getcwd()
for root, dirs, files in os.walk(".", topdown = False):
   for name in dirs:
    num = os.path.join(root, name)
    num = num.strip("./")
    os.chdir(cwd2 + "/" + str(num))
    lattice = read('POSCAR')
    length = len(lattice)
    Als = []
    ##### get direct position matrix from POSCAR without using ase function ##########################
    f = open('POSCAR', "r")
    line1 = f.readline() #Reads first line
    lattice_constant = float(f.readline()) #Reads second line 
    xx = [float(x) for x in f.readline().split()] #third line... etc.
    yy = [float(y) for y in f.readline().split()]
    zz = [float(z) for z in f.readline().split()]
    cell = np.array([xx, yy, zz]) * lattice_constant
    symbols = f.readline().split()
    numbers = [int(n) for n in f.readline().split()]
    total = sum(numbers)
    atomic_formula = ''.join('{:s}{:d}'.format(sym, numbers[n]) for n, sym in enumerate(symbols))
    #selective_dynamics = f.readline() # Move past this string towards the atom coordinates. Comment out this line if selective dynamics is not in CONTCAR
    direct = f.readline() # Another string line.
    atoms_pos = np.empty((total, 3))
    selective_flags = np.empty((total, 3), dtype=bool)
    for atom in range(total):
        ac = f.readline().split()
        atoms_pos[atom] = (float(ac[0]), float(ac[1]), float(ac[2]))
        #curflag = []
        #for flag in ac[3:6]:
        #    curflag.append(flag == 'F')
        #selective_flags[atom] = curflag
    pos = np.array(atoms_pos)
    a = LA.norm(xx) #Lattice parameters, angles in radians
    b = LA.norm(yy)
    c = LA.norm(zz)
    A = math.acos(np.dot(yy,zz)/b/c)
    B = math.acos(np.dot(xx,zz)/a/c)
    Y = math.acos(np.dot(xx,yy)/a/b)
##############################################################################################
    for i in range(length):
        if lattice.symbols[i] == 'Al':
            Als.append(i)
    Al_length = len(Als)
    cutOff = neighborlist.natural_cutoffs(lattice)
    neighborList = neighborlist.NeighborList(cutOff, self_interaction = False, bothways = True)
    neighborList.update(lattice)
    adj = neighborList.get_connectivity_matrix(sparse = False)
    adj_m = np.array(adj)
    Al_NN = [[],[],[],[]] #1st two are nearest O neighbors, 2nd two are second Si neighbors
    for x in range(Al_length):
        for i in range(length):
            if adj_m[Als[x]][i] == 1:
                Al_NN[x].append(i)
                for ii in range(length):
                    if adj_m[i][ii] == 1:
                        if ii == Als[x]:
                            continue
                        Al_NN[x+2].append(ii)
    N = len(Al_NN[0])
    Pd_pos = [[],[]]
    sc = 2.5 #scale factor
    for x in range(Al_length):
        for i in range(N):
            #Finds nearest reflection of O to Al
            delt = pos[Als[x]] - pos[Al_NN[x][i]]
            deltx1 = pos[Als[x]] - pos[Al_NN[x][i]]
            delty1 = pos[Als[x]] - pos[Al_NN[x][i]]
            deltz1 = pos[Als[x]] - pos[Al_NN[x][i]]
            deltx_1 = pos[Als[x]] - pos[Al_NN[x][i]]
            delty_1 = pos[Als[x]] - pos[Al_NN[x][i]]
            deltz_1 = pos[Als[x]] - pos[Al_NN[x][i]]
            deltx1[0] = delt[0] - 1
            delty1[1] = delt[1] - 1
            deltz1[2] = delt[2] - 1
            deltx_1[0] = delt[0] + 1
            delty_1[1] = delt[1] + 1
            deltz_1[2] = delt[2] + 1
            deltot = np.array([delt,deltx1,delty1,deltz1,deltx_1,delty_1,deltz_1])
            d = np.empty(7)
            k = 0
            for row in deltot:
                d[k] = math.sqrt(a**2*row[0]**2+b**2*row[1]**2+c**2*row[2]**2+2*b*c*math.cos(A)*row[1]*row[2]+2*c*a*math.cos(B)*row[2]*row[0]+2*a*b*math.cos(Y)*row[0]*row[1])
                k +=1
            d_min = np.argmin(d)
            if not d_min == 0:
                pos[Al_NN[x][i]]= pos[Als[x]] - deltot[d_min]
            #Finds nearest reflection of Si to O
            delt = pos[Al_NN[x][i]] - pos[Al_NN[x+2][i]]
            deltx1 = pos[Al_NN[x][i]] - pos[Al_NN[x+2][i]]
            delty1 = pos[Al_NN[x][i]] - pos[Al_NN[x+2][i]]
            deltz1 = pos[Al_NN[x][i]] - pos[Al_NN[x+2][i]]
            deltx_1 = pos[Al_NN[x][i]] - pos[Al_NN[x+2][i]]
            delty_1 = pos[Al_NN[x][i]] - pos[Al_NN[x+2][i]]
            deltz_1 = pos[Al_NN[x][i]] - pos[Al_NN[x+2][i]]
            deltx1[0] = delt[0] - 1
            delty1[1] = delt[1] - 1
            deltz1[2] = delt[2] - 1
            deltx_1[0] = delt[0] + 1
            delty_1[1] = delt[1] + 1
            deltz_1[2] = delt[2] + 1
            deltot = np.array([delt,deltx1,delty1,deltz1,deltx_1,delty_1,deltz_1])
            d = np.empty(7)
            k = 0
            for row in deltot:
                d[k] = math.sqrt(a**2*row[0]**2+b**2*row[1]**2+c**2*row[2]**2+2*b*c*math.cos(A)*row[1]*row[2]+2*c*a*math.cos(B)*row[2]*row[0]+2*a*b*math.cos(Y)*row[0]*row[1])
                k +=1
            d_min = np.argmin(d)
            if not d_min == 0:
                pos[Al_NN[x+2][i]]= pos[Al_NN[x][i]] - deltot[d_min]
            #calculates initial H positions
            pos[Al_NN[x][i]] = pos[Al_NN[x][i]] @ cell #convert to cartesian
            pos[Als[x]] = pos[Als[x]] @ cell
            pos[Al_NN[x+2][i]] = pos[Al_NN[x+2][i]] @ cell
            Pd_pos[x].append(((sc*(pos[Al_NN[x][i]]-pos[Als[x]])+pos[Als[x]])+(sc*(pos[Al_NN[x][i]]-pos[Al_NN[x+2][i]])+pos[Al_NN[x+2][i]]))/2)
            Pd_pos = np.array(Pd_pos)
            #calculates updated H positions
            D = 2.6 #desired O-H distance (Angstroms)
            HO = Pd_pos[x][i]-pos[Al_NN[x][i]]
            mc = D/np.linalg.norm(HO)
            Pd_pos[x][i] = HO*mc+pos[Al_NN[x][i]]
            cell_inv = np.linalg.inv(cell)
            Pd_pos[x][i] = Pd_pos[x][i] @ cell_inv #convert back to fractional
            pos[Al_NN[x][i]] = pos[Al_NN[x][i]] @ cell_inv
            pos[Als[x]] = pos[Als[x]] @ cell_inv
            pos[Al_NN[x+2][i]] = pos[Al_NN[x+2][i]] @ cell_inv
    pos_n = len(Pd_pos[0])
    adsorbate = Atoms('Na')
    lattice = lattice + adsorbate + adsorbate
    pos = lattice.get_scaled_positions()
    pos = np.array(pos)
    length = len(lattice)
    Pds = []
    for i in range(length):
        if lattice.symbols[i] == 'Na':
            Pds.append(i)
    Pd_length = len(Pds)
    for Pd in range(Pd_length):
        if Pd == 1:
            break
        for pos1 in range(pos_n):
            for pos2 in range(pos_n):
                if not os.path.exists(cwd + '/' + str(num) + ':' + str(pos1) + '_' + str(pos2)):
                    os.mkdir(cwd + '/' + str(num) + ':' + str(pos1) + '_' + str(pos2))
                os.chdir(cwd + '/'  + str(num) + ':' + str(pos1) + '_' + str(pos2))
                pos[Pds[Pd]] = Pd_pos[Pd][pos1]
                pos[Pds[Pd+1]] = Pd_pos[Pd+1][pos2]
                lattice.set_scaled_positions(pos)
                #lattice.set_distance(Pds[Pd], Al_NN[Pd][pos1], 0.98, fix=1)
                #lattice.set_distance(Pds[Pd+1], Al_NN[Pd+1][pos2], 0.98, fix=1)
                write_vasp('POSCAR', lattice, label = '', direct=True, sort=False, symbol_count=None, long_format=True)
                f = open('POSCAR', 'r')
                contents = f.readlines()
                f.close
                f = open('POSCAR', 'r')
                species = f.readline()
                f.close
                contents.insert(5,species)
                f = open('POSCAR', 'w')
                contents = ''.join(contents)
                f.write(contents)
                f.close
                os.chdir(cwd)
    #print(Pd_pos)
    #print(Al_pos[0][0])