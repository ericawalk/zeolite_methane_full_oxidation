#!/bin/sh
##SBATCH --constraint=UBHPC&CPU-Gold-6130&INTEL&u25&OPA&MRI #request 'skylake' nodes
##SBATCH --constraint=MRI|NIH #request 'cascade' and 'skylake' nodes
#SBATCH --partition=debug --qos=debug
##SBATCH --time=72:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
##SBATCH --constraint=IB
#SBATCH --mem=128000
##SBATCH --mem=23000
#SBATCH --job-name=2Nas_Plot
#SBATCH --output=job.out
#SBATCH --error=error.out
#SBATCH --mail-user=kevingie@buffalo.edu
#SBATCH --mail-type=ALL
##SBATCH --requeue
#Specifies that the job will be requeued after a node failure.
#The default is that the job will not be requeued.

module load python/py36-anaconda-5.3.1
############
module list

python Plot_energies.py