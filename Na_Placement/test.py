from ase import atom , Atoms
from ase import neighborlist
from ase.io import read
from scipy import sparse
import numpy as np
from numpy import linalg as LA
import sys
import os
from os.path import join
import re
import pandas as pd
import math
import seaborn as sns

cwd = os.getcwd()
energies = [] 
arrangements = []
Al_NN = []
Al_dist = []

lattice = read('CONTCAR')
length = len(lattice)
Als = []
count = 1
cutOff = neighborlist.natural_cutoffs(lattice)
l=len(cutOff)
cutOff[l-1] = 0.1
cutOff[l-2] = 0.1
print(cutOff)
neighborList = neighborlist.NeighborList(cutOff, self_interaction = False, bothways = True)
neighborList.update(lattice)
adj = neighborList.get_connectivity_matrix(sparse = False)