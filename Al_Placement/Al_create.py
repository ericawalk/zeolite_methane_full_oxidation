from ase import atom , Atoms
from ase.io import read
from ase.constraints import FixAtoms
from ase.io.vasp import write_vasp
import math
import os.path
import numpy as np
import os
import shutil

lattice = read('POSCAR')

total_Oxygen = np.arange(0,84);
total_Silicon = np.arange(84,120); 
length = len(total_Silicon)


cwd = os.getcwd()

#Loops through all Si atoms and replaces each with Al, excluding doubles, Lowensteins rule is not followed
for l in range(length):
    for k in range(length):

        if l == k:
            continue
        
        if os.path.exists(cwd + '/' + str(total_Silicon[k]) + '-' + str(total_Silicon[l])):
            continue
            
        lattice = read('POSCAR')
        lattice.symbols[total_Silicon[l]] = 'Al'
        lattice.symbols[total_Silicon[k]] = 'Al'
        
        #makes directory of each Al-Al case denoted by which Si was replaced
        if not os.path.exists(cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k])):
            os.mkdir(cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))

        #move to made directory from working directory of this script
        os.chdir(cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))

        #write POSCAR format of  Al-Al substituted zeolite framework
        write_vasp('POSCAR'.format(total_Silicon[l], total_Silicon[k]), lattice, label = '', direct=True, sort=True, symbol_count=None, long_format=True, vasp5=False)
        #edit POSCAR to add atom names --> vesta compatible
        f = open('POSCAR', 'r')
        contents = f.readlines()
        f.close
        contents.insert(5,'   Al    O   Si\n' )
        f = open('POSCAR','w')
        contents = ''.join(contents)
        f.write(contents)
        f.close()

        print("%s-%s is complete" % (total_Silicon[l], total_Silicon[k]), end='\r')  

        #copy INCAR, POTCAR, KPOINTS, and slurm script for each directory of Al-Al case
        #shutil.copy(cwd + '/' + 'INCAR', cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))
        #shutil.copy(cwd + '/' + 'POTCAR', cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))
        #shutil.copy(cwd + '/' + 'KPOINTS', cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))
        #shutil.copy(cwd + '/' + 'slurm_run_vasp.sh', cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))
        
        #move back to the working directory of this script
        os.chdir(cwd)
