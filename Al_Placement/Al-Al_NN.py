from ase import atom , Atoms
from ase import neighborlist
from scipy import sparse
from ase.io import read
import os
import numpy as np
import sys
from numpy import linalg as LA
import math
np.set_printoptions(threshold=sys.maxsize) #This is only used for de-bugging

lattice = read('CONTCAR')
length = len(lattice)
Als = []
count = 1
cutOff = neighborlist.natural_cutoffs(lattice)
neighborList = neighborlist.NeighborList(cutOff, self_interaction = False, bothways = True)
neighborList.update(lattice)
adj = neighborList.get_connectivity_matrix(sparse = False)

for i in range(length):
    if lattice.symbols[i] == 'Al':
        Als.append(i)

adj_m = LA.matrix_power(adj, count)
while adj_m[Als[0], Als[1]] == 0:
    count +=1
    adj_m = LA.matrix_power(adj, count)
#print(adj_m[Als[0], Als[1]])
#print(count)
d_adj = neighborlist.get_distance_matrix(adj, limit = 10)
#print(d_adj)

with open('CONTCAR') as f:
    line1 = f.readline()
    lat_const = f.readline()
    xx = [float(x) for x in f.readline().split()] #Lattice vectors
    yy = [float(y) for y in f.readline().split()]
    zz = [float(z) for z in f.readline().split()]
    symbols = f.readline().split()
    numbers = [int(n) for n in f.readline().split()]
    direct = f.readline()
    total = sum(numbers)
    atoms_pos = np.empty((total, 3))
    for atom in range(total):
        ac = f.readline().split()
        atoms_pos[atom] = (float(ac[0]), float(ac[1]), float(ac[2]))
    
a = LA.norm(xx) #Lattice parameters, angles in radians
b = LA.norm(yy)
c = LA.norm(zz)
A = math.acos(np.dot(yy,zz)/b/c)
B = math.acos(np.dot(xx,zz)/a/c)
Y = math.acos(np.dot(xx,yy)/a/b)
delt = atoms_pos[Als[0]] - atoms_pos[Als[1]]
deltx1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
delty1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
deltz1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
deltx_1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
delty_1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
deltz_1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
deltx1[0] = delt[0] - 1
delty1[1] = delt[1] - 1
deltz1[2] = delt[2] - 1
deltx_1[0] = delt[0] + 1
delty_1[1] = delt[1] + 1
deltz_1[2] = delt[2] + 1
deltot = np.array([delt,deltx1,delty1,deltz1,deltx_1,delty_1,deltz_1])
d = np.empty(7)
k = 0
for row in deltot:
    d[k] = math.sqrt(a**2*row[0]**2+b**2*row[1]**2+c**2*row[2]**2+2*b*c*math.cos(A)*row[1]*row[2]+2*c*a*math.cos(B)*row[2]*row[0]+2*a*b*math.cos(Y)*row[0]*row[1])
    k +=1
d_min = np.amin(d)
print(d_min)

