from ase import atom , Atoms
from ase.io import read
from ase.constraints import FixAtoms
from ase.io.vasp import write_vasp
import math
import os.path
import numpy as np
import os
import shutil

lattice = read('POSCAR')

total_Oxygen = np.arange(0,84);
Silicon1 = [101,105,106,107];
Silicon2 = [79,81,83,85,87,89,91,93,95,97,99,101,103,105,107];
Is_Symmetric = np.array([[1,0,0,0],[1,0,1,0],[1,0,1,0],[0,1,1,0],[0,0,1,0],[0,0,1,0],[1,0,0,0],[1,0,1,0],[1,1,1,1],[0,0,1,0],[0,0,1,0],[0,0,1,0],[1,0,0,0],[1,0,1,0],[1,1,1,0]])
length1 = len(Silicon1)
length2 = len(Silicon2)


cwd = os.getcwd()

#Loops through Silicon 1 & 2, only creates a POSCAR if there is a 1 in the Is_Symmetric matrix for each specific combination
for l in range(length1):
    for k in range(length2):
        if not Is_Symmetric[k][l] == 1:
            continue
    
        lattice = read('POSCAR')
        lattice.symbols[Silicon1[l]] = 'Al'
        lattice.symbols[Silicon2[k]] = 'Al'
        
        #makes directory of each Al-Al case denoted by which Si was replaced
        if not os.path.exists(cwd + '/' + str(Silicon1[l]) + '-' + str(Silicon2[k])):
            os.mkdir(cwd + '/' + str(Silicon1[l]) + '-' + str(Silicon2[k]))

        #move to made directory from working directory of this script
        os.chdir(cwd + '/' + str(Silicon1[l]) + '-' + str(Silicon2[k]))

        #write POSCAR format of  Al-Al substituted zeolite framework
        write_vasp('POSCAR'.format(Silicon1[l], Silicon2[k]), lattice, label = '', direct=True, sort=True, symbol_count=None, long_format=True, vasp5=False)
        #edit POSCAR to add atom names --> vesta compatible
        f = open('POSCAR', 'r')
        contents = f.readlines()
        f.close
        contents.insert(5,'   Al    O   Si\n' )
        f = open('POSCAR','w')
        contents = ''.join(contents)
        f.write(contents)
        f.close()

        print("%s-%s is complete" % (Silicon1[l], Silicon2[k]), end='\r')  

        #copy INCAR, POTCAR, KPOINTS, and slurm script for each directory of Al-Al case
        shutil.copy(cwd + '/' + 'INCAR', cwd + '/' + str(Silicon1[l]) + '-' + str(Silicon2[k]))
        shutil.copy(cwd + '/' + 'POTCAR', cwd + '/' + str(Silicon1[l]) + '-' + str(Silicon2[k]))
        shutil.copy(cwd + '/' + 'KPOINTS', cwd + '/' + str(Silicon1[l]) + '-' + str(Silicon2[k]))
        shutil.copy(cwd + '/' + 'slurm_run_vasp.sh', cwd + '/' + str(Silicon1[l]) + '-' + str(Silicon2[k]))
        
        #move back to the working directory of this script
        os.chdir(cwd)
