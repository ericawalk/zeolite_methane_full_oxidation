#!/bin/sh
##SBATCH --partition=general-compute --qos=general-compute
##SBATCH --partition=largemem
#SBATCH --partition=debug --qos=debug
##SBATCH --partition=skylake --qos=skylake
#SBATCH --cluster=ub-hpc
##SBATCH --cluster=chemistry
#SBATCH --time=01:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
##SBATCH --constraint=IB
#SBATCH --mem=128000
##SBATCH --mem=23000
#SBATCH --job-name=Al_create
#SBATCH --output=job.out
#SBATCH --error=error.out
##SBATCH --mail-user=kevingie@buffalo.edu
#SBATCH --mail-type=ALL
##SBATCH --requeue
#Specifies that the job will be requeued after a node failure.
#The default is that the job will not be requeued.

module load python/py36-anaconda-5.3.1

srun python Al_create.py
