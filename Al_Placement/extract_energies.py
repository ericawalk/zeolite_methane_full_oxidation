import os
from os.path import join
import re
import numpy as np
import pandas as pd
cwd = os.getcwd()
energies = [] 
arrangements = []
for root, dirs, files, in os.walk(cwd):
    for name in dirs:
        os.chdir(join(root, name))
        with open('OUTCAR') as f:
            content = f.readlines()
            rcontent = reversed(content)
        for line in rcontent:
            line = line.strip()
            if 'sigma' in line:
                nums = re.findall(r"[-+]?\d*\.\d+|\d+", line)
                num = float(nums[-1])
                break
        energies.append(num)
        arrangements.append(name)
os.chdir(cwd)
data = list(zip(arrangements, energies))
ddata = pd.DataFrame(data, columns = ['arrangement', 'energy'])
plot = ddata.plot(x = 'arrangement', y= 'energy', kind = 'scatter')
plot.figure.savefig('plot.png')
print(ddata)
