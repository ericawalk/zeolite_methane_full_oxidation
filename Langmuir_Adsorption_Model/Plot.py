import numpy as np
import sys
import os
import os.path
from os.path import join
import re
import pandas as pd
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

cwd = os.getcwd()
step = 31
P_O2_min = 0.01 #partial pressure of oxygen range
P_O2_max = 0.21
in_P = (P_O2_max-P_O2_min)/(step-1)
T_min = 200+273.15 #Temp range
T_max = 500+273.15
in_T = (T_max-T_min)/(step-1)
P_O2_range = np.arange(P_O2_min,P_O2_max,in_P)
P_O2_range = np.append(P_O2_range,P_O2_max)
T_range = np.arange(T_min,T_max,in_T)
T_range = np.append(T_range,T_max)
bridge_coverage = []
single_coverage = []
count=0
countb=0
for T in T_range:
    for P_O2 in P_O2_range:

        #Extract DFT energies
        DFT_energy = {}
        for root, dirs, files, in os.walk(cwd):
            for name in dirs:
                try:
                    if name == "vtst":
                        continue
                    os.chdir(join(root, name)) 
                    with open('OUTCAR') as f:
                        content = f.readlines()
                        rcontent = reversed(content)
                    for line in rcontent:
                        line = line.strip()
                        if 'sigma' in line:
                            nums = re.findall(r"[-+]?\d*\.\d+|\d+", line)
                            num = float(nums[-1])
                            DFT_energy[name] = num
                            break
                except:
                    pass
        
        #Extract ZPVE and qvib
        ZPVE = {}
        qvib = {}
        S_T = {}
        for root, dirs, files, in os.walk(cwd):
            try:
                name = str(root)
                name = name[len(cwd)+1:]
                name = name[:-5]
                os.chdir(root)
                if not os.path.isfile('freq.dat'):
                    os.system('/projects/academic/mdupuis2/software/vtst/vtsttools/vtstscripts/dymmatrix.pl')
                freq = []
                with open('freq.dat') as f:
                    content = f.readlines()
                    for line in content:
                        line = line.strip()
                        nums = re.findall(r"[-+]?\d*\.\d+|\d+", line)
                        freq.append(float(nums[0]))
                    freq = np.array(freq)
                    l = len(freq)
                    for k in range(l):
                        if freq[k] < 100:
                            freq[k] = 100
                    c = 30000000000
                    kb = 0.00008617
                    h = 0.00000000000000413566
                    freq = freq * c
                    zpe = .5*h*freq
                    ZPVE[name] = np.sum(zpe)
                    Qvib = 1/(1-np.exp((-h*freq)/(kb*T)))
                    qvib[name] = np.prod(Qvib)
                    S_T[name] = kb*T*np.log(qvib[name])
            except:
                pass
        os.chdir(cwd)
        
        #Entropy corrections of select molecules (eV)
        S_T['O2'] = (31.32234*np.log(T/1000)-20.23531*(T/1000)+57.86644/2*(T/1000)**2-36.50624/3*(T/1000)**3+0.007374/(2*(T/1000)**2)+246.7945)/96.485/1000*T
        #Total partition function of select molecules
        
        #Calculate free energies of species
        G_energy = {}
        for key in DFT_energy:
            G_energy[key] = DFT_energy[key]+ZPVE[key]-S_T[key]
        
        #Calculate free energy of formation of active sites
        #SUB = str.maketrans("0123456789", "₀₁₂₃₄₅₆₇₈₉")
        Formation_energy = {}
        Formation_energy['ZPdOPdZ'] = G_energy['2_4-PdOPd']-G_energy['2_4-Blank']-0.5*G_energy['O2']
        Formation_energy['ZPdO2PdZ'] = G_energy['2_4-PdO2Pd']-G_energy['2_4-Blank']-G_energy['O2']
        Formation_energy['(ZPdO)2'] = G_energy['2_4-PdOx2']-G_energy['2_4-Blank']-G_energy['O2']
        Formation_energy['ZPdO-OPdZ'] = G_energy['2_4-PdO2Pdv2']-G_energy['2_4-Blank']-G_energy['O2']
        Formation_energy['ZOPdO2PdZ'] = G_energy['2_4-OPdO2Pd']-G_energy['2_4-Blank']-1.5*G_energy['O2']
        Formation_energy['(ZPdO2)2'] = G_energy['2_4-PdO2x2']-G_energy['2_4-Blank']-2*G_energy['O2']
        Formation_energy['ZPdO'] = G_energy['1_3-PdOx2']-G_energy['1_3-Blank']-G_energy['O2']
        Formation_energy['ZPdO2'] = G_energy['1_3-PdO2x2']-G_energy['1_3-Blank']-2*G_energy['O2']
        
        #Calculate equilibrium constants of active site formations
        K_sites = {}
        for key in Formation_energy:
            K_sites[key] = np.exp(Formation_energy[key]/(-kb*T))
        
        #Calculate coverage fractions of bridged active sites
        theta_bridge = {}
        theta_bridge['ZPdOPdZ'] = (K_sites['ZPdOPdZ']*np.sqrt(P_O2))/(1+K_sites['ZPdOPdZ']*np.sqrt(P_O2)+K_sites['ZPdO2PdZ']*P_O2+K_sites['ZPdO-OPdZ']*P_O2+K_sites['ZOPdO2PdZ']*(P_O2**1.5))
        theta_bridge['ZPdO2PdZ'] = (K_sites['ZPdO2PdZ']*P_O2)/(1+K_sites['ZPdOPdZ']*np.sqrt(P_O2)+K_sites['ZPdO2PdZ']*P_O2+K_sites['ZPdO-OPdZ']*P_O2+K_sites['ZOPdO2PdZ']*(P_O2**1.5))
        theta_bridge['ZPdO-OPdZ'] = (K_sites['ZPdO-OPdZ']*P_O2)/(1+K_sites['ZPdOPdZ']*np.sqrt(P_O2)+K_sites['ZPdO2PdZ']*P_O2+K_sites['ZPdO-OPdZ']*P_O2+K_sites['ZOPdO2PdZ']*(P_O2**1.5))
        theta_bridge['ZOPdO2PdZ'] = (K_sites['ZOPdO2PdZ']*(P_O2**1.5))/(1+K_sites['ZPdOPdZ']*np.sqrt(P_O2)+K_sites['ZPdO2PdZ']*P_O2+K_sites['ZPdO-OPdZ']*P_O2+K_sites['ZOPdO2PdZ']*(P_O2**1.5))
        theta_bridge['ZPd2'] = 1/(1+K_sites['ZPdOPdZ']*np.sqrt(P_O2)+K_sites['ZPdO2PdZ']*P_O2+K_sites['ZPdO-OPdZ']*P_O2+K_sites['ZOPdO2PdZ']*(P_O2**1.5))
        TP = [T,P_O2]
        TP2 = [T,P_O2]
        bridge_coverage.append(TP)
        single_coverage.append(TP2)
        for key in theta_bridge:
            bridge_coverage[count].append(theta_bridge[key])
        count = count +1
        #Calculate coverage fractions of single active sites
        theta_single = {}
        theta_single['ZPdO2'] = (K_sites['ZPdO2']*P_O2)/(1+K_sites['ZPdO2']*P_O2+K_sites['ZPdO']*np.sqrt(P_O2))
        theta_single['ZPdO'] = (K_sites['ZPdO']*np.sqrt(P_O2))/(1+K_sites['ZPdO2']*P_O2+K_sites['ZPdO']*np.sqrt(P_O2))
        theta_single['ZPd'] = 1/(1+K_sites['ZPdO2']*P_O2+K_sites['ZPdO']*np.sqrt(P_O2))
        print()
        for key in theta_single:
            single_coverage[countb].append(theta_single[key])
        countb = countb +1
bridge_coverage = pd.DataFrame(data=bridge_coverage, columns = ['T','P_O2','ZPdOPdZ','ZPdO2PdZ','ZPdO-OPdZ','ZOPdO2PdZ','ZPd2'])
bridge_coverage.to_excel("bridge_coverage.xlsx")
single_coverage = pd.DataFrame(data=single_coverage, columns = ['T','P_O2','ZPdO2','ZPdO','ZPd'])
single_coverage.to_excel("single_coverage.xlsx")

#fig = plt.figure()
#ax = Axes3D(fig)
#for col in bridge_coverage:
#    if col == 'T':
#        continue
#    if col == 'P_O2':
#        continue
#    plot = ax.plot_trisurf(bridge_coverage['T'], bridge_coverage['P_O2'], bridge_coverage[col], label = key)
#plot._facecolors2d=plot._facecolors3d
#plot._edgecolors2d=plot._edgecolors3d
#ax.invert_xaxis()
#ax.set_xlabel("T (K)")
#ax.set_ylabel("P_O2")
#ax.set_zlabel("Coverage")
#plot.legend()
#plt.savefig('bridge_coverage.png')